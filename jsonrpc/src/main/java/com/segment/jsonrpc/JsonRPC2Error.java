package com.segment.jsonrpc;

public class JsonRPC2Error {
    private int code;
    private String message;
    private Object data;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

    public static class ErrorException extends RuntimeException {
        private JsonRPC2Error errorResponse;

        public ErrorException(JsonRPC2Error error) {
            super(error.message);
            errorResponse = error;
        }

        public JsonRPC2Error getErrorResponse() {
            return errorResponse;
        }
    }
}
